// parameters for final rendering
//$fa = 1;
//$fs = 0.4;

bottom_plate_radius = 105;
bottom_plate_height = 5;
bottom_plate_edge_height = 5.2;
bottom_plate_edge_thickness = 4.8;
bottom_plate_slot_depth = 2;
bottom_plate_slot_width = 3.6;
bottom_plate_hole_radius = 10;

// bottom plate design

union(){
    difference(){
        // the plate
        cylinder(h=bottom_plate_height, r=bottom_plate_radius, center = true);
        //body slot
        difference(){
            translate([0,0,bottom_plate_height-bottom_plate_slot_depth])
                cylinder(h=bottom_plate_slot_depth, r =bottom_plate_radius - bottom_plate_edge_thickness, center = true);
            translate([0,0,bottom_plate_height-bottom_plate_slot_depth])
                cylinder(h=bottom_plate_slot_depth, r =bottom_plate_radius - bottom_plate_edge_thickness - bottom_plate_slot_width, center = true);
        }
        //holes in the bottom plate
        cylinder(h = bottom_plate_height, r = bottom_plate_hole_radius, center = true);

        for (i=[1:1:2]){
            n = 8 +(i-1)*6;
            step = 360/n;
            r = 40*i;
            for (angle=[0:step:359]){
                dx = r*cos(angle);
                dy = r*sin(angle);
                translate([dx,dy,0])
                    cylinder(h = bottom_plate_height, r = bottom_plate_hole_radius, center = true);
            }
        } 
    }
    

    // the edge
    difference(){
        translate([0,0,bottom_plate_height])
            cylinder(h=bottom_plate_edge_height, r=bottom_plate_radius,center = true);
        translate([0,0,bottom_plate_height])    
            cylinder(h=bottom_plate_edge_height, r=bottom_plate_radius - bottom_plate_edge_thickness, center = true);
    }
}

