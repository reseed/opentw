# openTW

OpenTW is an open-hardware initiative for treatment wetland monitoring.
## Context

Treatment wetlands are engineered systems designed to mimic natural environments in order to effciently treat various types of polluted water. They are used worldwide and have gained increasing popularity during recent decades. 
The scientific knowledge on treatment wetlands is mainly based on a cumulative model and therefore will gain if scientists were able to effeciently share data as well as methodological development including sensors.
The problem is particularly accute for field experiments that require:
- rough sensors capable of operating in harsh environmental conditions,
- low-energy consumption sensors featured for off-grid power solutions,
- low-cost sensors that could be extensively deployed for better representativity,
- sensors tailored to the specificities of treatment wetlands.
This set of specifications has led us to consider open hardware solutions to fulfill them.

> "I do not know of a field of science where knowledge has increased cumulatively that has not been basically open." *C. Nelson*

Open hardware can be considered as a subset of the open-design movement. While collective invention seemed to have fuelled most of the early industrial period, it has desepeared in front of the patent system to only be resurected in the late 70's by the emerging software industry.

Open-design is a co-creation process of machine and systems through the use of publicly shared design information protected by a viral licensing (what has been openly open should be openly shared).

OpenTW aims at hosting the development of open hardware instruments for the monitoring of treatment wetlands. It is a collaborative efforts to which anyone is invited to participate under the sole condition of respecting the licensing conditions. The maintainers of openTW hopes it will act as a springboard to develop new sensors and to share more actively the data obtained from it.